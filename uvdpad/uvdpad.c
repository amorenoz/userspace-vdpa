/*
 * Copyright (C) 2020  - All Rights Reserved
 * Author: Maxime Coquelin <maxime.coquelin@redhat.com>
 *
 * TBD: Licencing
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <wordexp.h>

#include <rte_eal.h>
#include <rte_vdpa.h>
#include <rte_vhost.h>

#include "uvdpad.h"
#include "jsonrpc.h"

#define VERSION "0.1"
#define JSON_SOCKET_PATH "/var/run/uvdpad.sock"

const char *eal_args = "uvdpad -m 32 --no-huge -w 0000:00.0";
static struct uvdpad uvdpa;


static int
vdpa_new_device(int vid)
{
	char ifname[256];

	rte_vhost_get_ifname(vid, ifname, sizeof(ifname));

	fprintf(stdout, "New connection on %s\n", ifname);

	return 0;
}

static void
vdpa_destroy_device(int vid)
{
	char ifname[256];

	rte_vhost_get_ifname(vid, ifname, sizeof(ifname));

	fprintf(stdout, "Disconnection on %s\n", ifname);

	return;
}

static const struct vhost_device_ops vdpa_notif_ops = {
	.new_device = vdpa_new_device,
	.destroy_device = vdpa_destroy_device,
};

static int method_version(json_t *json_params, json_t **result, void *data)
{
	(void)(json_params);
	(void)(data);

	*result = json_string(VERSION);

	return 0;
}

static int method_list_methods(json_t *json_params, json_t **result,
								void *data)
{
	struct jsonrpc_method_entry_t *method;
	json_t *method_string;

	(void)(json_params);
	(void)(data);

	*result = json_array();

	for (method = uvdpa.socket.jsonrpc_methods; method->name; method++) {
		method_string = json_string(method->name);
		if (json_array_append_new(*result, method_string))
			fprintf(stderr, "Failed to append to array\n");
	}

	return 0;
}

static int method_create_iface(json_t *json_params, json_t **result,
							   void *data)
{
	char *dev_id, *sock_path, *mode, devargs[256];
	json_error_t error;
	size_t flags = 0;
	uint64_t vhost_flags = 0;
	struct rte_vdpa_dev_addr addr;
	int vdpa_did, ret;

	(void)(data);

	if (!json_is_object(json_params)) {
		*result = jsonrpc_error_object_predefined(JSONRPC_INVALID_PARAMS,
												  NULL);
		return -1;
	}

	ret = json_unpack_ex(json_params, &error, flags, "{s:s,s:s,s:s}",
			"device-id", &dev_id,
			"socket-path", &sock_path,
			"socket-mode", &mode);
	if (ret) {
		*result = jsonrpc_error_object_predefined(JSONRPC_INVALID_PARAMS,
												  json_string(error.text));
	}

	if (!strncmp(mode, "client", strlen("client"))) {
		vhost_flags |= RTE_VHOST_USER_CLIENT;
	} else if (strncmp(mode, "server", strlen("server"))) {
		*result = jsonrpc_error_object_predefined(JSONRPC_INVALID_PARAMS,
												  NULL);
		return -1;
	}

	if (!(vhost_flags & RTE_VHOST_USER_CLIENT) &&
			access(sock_path, F_OK) != -1) {
		*result = jsonrpc_error_object_predefined(JSONRPC_INVALID_PARAMS,
								json_string(strerror(EEXIST)));
		return -1;
	}

	//FIXME: Only PCI support for now
	ret = rte_pci_addr_parse(dev_id, &addr.pci_addr);
	if (ret) {
		*result = jsonrpc_error_object_predefined(JSONRPC_INVALID_PARAMS,
								json_string("Invalid device address"));
		return -1;
	}
	addr.type = PCI_ADDR;

	snprintf(devargs, sizeof(devargs), "%s,vdpa=1", dev_id);

	ret = rte_dev_probe(devargs);
	if (ret) {
		*result = jsonrpc_error_object_predefined(JSONRPC_INVALID_PARAMS,
								json_string("Device hotplug failed"));
		return -1;
	}

	vdpa_did = rte_vdpa_find_device_id(&addr);
	if (vdpa_did < 0) {
		*result = jsonrpc_error_object_predefined(JSONRPC_INVALID_PARAMS,
								json_string("Failed to find vDPA device"));
		return -1;
	}

	ret = rte_vhost_driver_register(sock_path, vhost_flags);
	if (ret) {
		*result = jsonrpc_error_object_predefined(JSONRPC_INVALID_PARAMS,
								json_string("Vhost register failed"));
		return -1;
	}

	ret = rte_vhost_driver_callback_register(sock_path, &vdpa_notif_ops);
	if (ret) {
		*result = jsonrpc_error_object_predefined(JSONRPC_INVALID_PARAMS,
								json_string("Vhost callback register failed"));
		return -1;
	}

	ret = rte_vhost_driver_attach_vdpa_device(sock_path, vdpa_did);
	if (ret) {
		*result = jsonrpc_error_object_predefined(JSONRPC_INVALID_PARAMS,
								json_string("Failed to attach to vDPA device"));
		return -1;
	}

	ret = rte_vhost_driver_start(sock_path);
	if (ret) {
		*result = jsonrpc_error_object_predefined(JSONRPC_INVALID_PARAMS,
								json_string("Failed to start Vhost port"));
		return -1;
	}

	fprintf(stdout, "devid: %s, path: %s, mode: %s\n", dev_id, sock_path, mode);

	return 0;
}

static struct jsonrpc_method_entry_t uvdpa_methods[] = {
	{ "version", method_version, "" },
	{ "list-methods", method_list_methods, "" },
	{ "create-interface", method_create_iface, "o" },
	{ NULL },
};

int main(void)
{
	wordexp_t eal;
	char json_socket_path[PATH_MAX];
	int ret;

	ret = wordexp(eal_args, &eal, WRDE_NOCMD);
	if (ret) {
		fprintf(stderr, "EAL args parsing failed (%d)\n", ret);
		exit(1);
	}

	ret = rte_eal_init(eal.we_wordc, eal.we_wordv);
	if (ret < 0) {
		fprintf(stderr, "EAL init failed (%d)\n", ret);
		exit(1);
	}

	/* ToDo: check for alternative in cmdline or cfg file. */
	strncpy(json_socket_path, JSON_SOCKET_PATH, sizeof(json_socket_path));
	uvdpa.socket.path = json_socket_path;
	ret = unix_socket_create(&uvdpa.socket);
	if (ret) {
		fprintf(stderr, "Failed to create JSON-RPC socket (%d)\n", ret);
		exit(1);
	}

	ret = unix_socket_register_json(&uvdpa.socket, uvdpa_methods);
	if (ret) {
		fprintf(stderr, "Failed to register JSON-RPC handlers (%d)\n", ret);
		exit(1);
	}

	fprintf(stdout, "JSON-RPC socket created (%s)\n", JSON_SOCKET_PATH);

	while(1) {
		ret = unix_socket_process(&uvdpa.socket);
		if (ret)
			break;
	}

	return 0;
}
