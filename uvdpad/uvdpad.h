/*
 * Copyright (C) 2020  - All Rights Reserved
 * Author: Maxime Coquelin <maxime.coquelin@redhat.com>
 *
 * TBD: Licencing
 */

#ifndef __UVDPAD_H
#define __UVDPAD_H

#include "unix-socket.h"

struct uvdpad {
	struct unix_socket socket;
};

#endif /* __UVDPAD_H */
